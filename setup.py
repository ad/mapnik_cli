# -*- coding: utf-8 -*-

"""
Setup for building and installing mapnik_cli package.
"""

import sys
from setuptools import setup

with open('README.md') as f:
    LONG_DESCRIPTION = f.read()

try:
    from semantic_release import setup_hook
    setup_hook(sys.argv)
except ImportError:
    pass

VERSION_NUMBER = {}
with open("mapnik_cli/version.py") as fp:
    exec(fp.read(), VERSION_NUMBER)  # pylint: disable=W0122

setup(name="mapnik_cli",
      version=VERSION_NUMBER['__version__'],
      description="A simple mapnik command line interface (cli)",
      long_description=LONG_DESCRIPTION,
      long_description_content_type='text/markdown',
      author='Alexander Dunkel',
      author_email='alexander.dunkel@tu-dresden.de',
      url='https://gitlab.vgiscience.de/ad/mapnik_cli',
      license='GNU GPLv3 or any higher',
      packages=['mapnik_cli'],
      include_package_data=True,
      python_requires='>=3.7',  # minimum, due to dataclass
      install_requires=[
          'mapnik',
      ],
      entry_points={
          'console_scripts': [
              'mapnik_cli = mapnik_cli.__main__:main'
          ]
      })
