
# mapnik_cli

A simple mapnik command line interface (cli)

**mapnik_cli** provides a simple command line interface to python mapnik bindings.

## Installation

Make sure you have mapnik python bindings installed.
```bash
git clone https://gitlab.vgiscience.de/ad/mapnik_cli
cd mapnik_cli
pip install --no-dependencies --editable .
```

## Arguments

See the [mapnik_cli/config/config.py](mapnik_cli/config/config.py) Module for a documentation of command line arguments.

## Example

```bash
mapnik_cli \
    --stylesheet_name "sample_style.xml" \
    --output_name "sample_map.png" \
    --map_dimensiony_x 2500 \
    --map_dimensiony_y 1400 \
    --input_path "/mapnik-test" \
    --output_path "/map"
```