# -*- coding: utf-8 -*-
"""A simple mapnik_cli map render package"""

from __future__ import absolute_import

from mapnik_cli.version import __version__