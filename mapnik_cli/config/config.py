# -*- coding: utf-8 -*-

"""
Config processing for mapnik_cli package.
"""

from __future__ import absolute_import

import argparse
from pathlib import Path
from mapnik_cli.version import __version__


class BaseConfig:
    """Optional class for handling of typical config parameters"""

    def __init__(self):
        # Set Default Config options here
        # or define options as input args
        self.font_source = "/fonts"
        self.stylesheet_name = ""
        self.output_name = ""
        self.map_dimensiony_x = 100
        self.map_dimensiony_y = 100
        self.map_bounds = None
        self.list_fontfaces = False
        self.input_path = Path.cwd()
        self.output_path = Path.cwd()
        self.bbox = None

        self.parse_args()

    def parse_args(self):
        """Parse init args and set default values

        Note: All paths are relative.
        """
        parser = argparse.ArgumentParser()
        parser.add_argument('--version',
                            action='version',
                            version=f'mapnik_cli {__version__}')
        parser.add_argument("-s", "--stylesheet_name", type=str,
                            help=
                            "Specify name of the XML stylesheet to use "
                            "(without the path)")
        parser.add_argument("-o", "--output_name", type=str,
                            help="Specify the output name of the generated map "
                            "(without the path)")
        parser.add_argument("--map_dimensiony_x", type=int,
                            help="Specify the x dimension in pixels of the map.")
        parser.add_argument("--map_dimensiony_y", type=int,
                            help="Specify the y dimension in pixels of the map.")
        parser.add_argument("--map_bounds", type=str,
                            help="Optionally provide bounds to clip the map. Will "
                            "zoom to extent by default.")
        parser.add_argument("--list_fontfaces",
                            action="store_true",
                            help="If set, will output a lit of available font faces.")
        parser.add_argument("--input_path",
                            type=Path,
                            help="Input base path to use. By default, this will "
                            "be the current directory.")
        parser.add_argument("--output_path",
                            type=Path,
                            help="Output base path to use. By default, this will "
                            "be the current directory.")
        parser.add_argument("--bbox",
                            type=str,
                            help="Set the bounding box for the map. Format: "
                            "minx,miny,maxx,maxy. "
                            "See Shapely shape.bounds [1][1]. "
                            "Default: Zoom to extent. "
                            "    "
                            "[1]: https://shapely.readthedocs.io/en/maint-1.8/manual.html#object.bounds")
        args = parser.parse_args()

        if args.stylesheet_name:
            self.stylesheet_name = args.stylesheet_name
        if args.output_name:
            self.output_name = args.output_name
        if args.map_dimensiony_x:
            self.map_dimensiony_x = args.map_dimensiony_x
        if args.map_dimensiony_y:
            self.map_dimensiony_y = args.map_dimensiony_y
        if args.map_bounds:
            self.map_bounds = args.map_bounds
        if args.list_fontfaces:
            self.list_fontfaces = True
        if args.input_path:
            self.input_path = args.input_path
        if args.output_path:
            self.output_path = args.output_path
        if args.bbox:
            bbox_tuple = args.bbox.lstrip("(").rstrip(")").replace(" ","").split(",")
            bbox_tuple = tuple(float(x) for x in bbox_tuple)
            if len(bbox_tuple) != 4:
                raise ValueError(
                    'Malformed bbox: 4 Coordinates needed, separated by comma.')
            self.bbox = bbox_tuple