#!/usr/bin/env python3

"""A simple mapnik_cli, command line interface (cli) package"""

from __future__ import absolute_import
# delay evaluation of annotations at runtime (PEP 563)
from __future__ import annotations

__author__ = "Alexander Dunkel"
__license__ = "GNU GPLv3"

import mapnik
from pathlib import Path
from mapnik_cli.config.config import BaseConfig


def main():
    """Main method for direct execution of package."""

    cfg = BaseConfig()

    mapnik.register_fonts(cfg.font_source)
    if cfg.list_fontfaces:
        for face in mapnik.FontEngine.face_names(): print(face)

    m = mapnik.Map(cfg.map_dimensiony_x, cfg.map_dimensiony_y)
    mapnik.load_map(m, str(Path(cfg.input_path / cfg.stylesheet_name)))
    if cfg.bbox:
        bbox = mapnik.Box2d(*cfg.bbox)
        m.zoom_to_box(bbox)
    else:
        m.zoom_all()
    mapnik.render_to_file(m, str(Path(cfg.output_path / cfg.output_name)))


if __name__ == "__main__":
    main()